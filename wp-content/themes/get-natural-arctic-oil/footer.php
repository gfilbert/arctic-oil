<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Get_Natural_Arctic_Oil
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info">
		<?php
		wp_nav_menu( array( 
			'theme_location' => 'footer-menu', 
			'container_class' => 'footer-nav' ) ); 
		?>
			<img src="https://getnaturalarcticoil.com/wp-content/uploads/2019/10/natural-arctic-oil-custom-badges-01.png" width="80px">
			<p>These statement have not been evaluated by the Food and Drug Administraton. This product is not intended to diagnose, treat, cure or prevent any disease.</p>
			<p>Photo Disclaimer: All photos on this website are stock photography and do not depict any actual consumers.</p>
			<p>&copy; Arctic Health Solutions 2019<span class="sep"> | </span>All Rights Reserved</p>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
